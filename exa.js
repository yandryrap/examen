const formulario = document.getElementById('formulario');
const inputs=document.querySelectorAll('#formulario input');

const expresiones = {
	codigo: /^[a-z0-9_-]{1,5}$/, // Letras y espacios, pueden llevar acentos.
	marca: /^[a-z0-9_-]{1,50}$/,
	modelo: /^[a-z0-9_-]{1,30}$/,
	año: /^\d{4}$/,
		
}



const campos = {
	fecha_inicial: false,
	fecha_final: false,
	codigo: false,
	año: false,
	marca: false,
	modelo: false
	
}
const ValidarFormulario =(e)=>{
 
switch(e.target.name) {
	case "codigo":
		if(expresiones.codigo.test(e.target.value)){
			
			campos['codigo'] = true;
			
		}else{
			alert("El marca debe de ser minimo 1 y maximo 5caracteres");
			campos['codigo'] = false;
			
		}
		break;
	case "marca":
		if(expresiones.marca.test(e.target.value)){
		
			campos['marca'] = true;
		}else{
			
			alert("El marca debe de ser minimo 1 y maximo 50 caracteres");
			campos['marca'] = false;
		}
		break;
		case "modelo":
			if(expresiones.modelo.test(e.target.value)){
				
				campos['modelo'] = true;
			}else{
				alert("El modelo debe de ser minimo 1 y maximo 30 caracteres");
				campos['modelo'] = false;
			}
			break;
			case "año":
			if(expresiones.año.test(e.target.value)){
				
				campos['año'] = true;
			}else{
				alert("El año debe de ser minimo 1 y maximo 4 caracteres");
				campos['año'] = false;
			}
			break;
			
		case "fecha_final":
			var fecha_1 = new Date(document.getElementById("fecha_inicial").value) ;
    var fecha_2 = new Date(document.getElementById("fecha_final").value) ;
	
		
	if(fecha_1>fecha_2){
	
		alert("La fecha de final debe ser mayor a la inicial ");
		return false;
	}else{
	
		return true;
		
	}
			break;			
	};
}




inputs.forEach((input) => {
	input.addEventListener('keyup',ValidarFormulario);
	input.addEventListener('blur', ValidarFormulario);
});

formulario.addEventListener('submit',(e) =>{
e.preventDefault();
});